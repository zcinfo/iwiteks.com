---
title: "id生成算法与类库选择"
date: "2018-06-12T13:50:46+02:00"
tags: ["java", "database"]
categories: ["blog"]
author: "龙见"
---

实体数据在持久化时，需要通过标识来进行操作，在数据库持久化中通常使用数据表实体的主键来进行标识，如mysql的自增字段。另外，除数据持久化外，其它场景也有可能生成唯一id，用作标识。

id生成有较多策略，如：
1. 数据库自增（优点：简单易用，不用单独维护；缺点：迁移性差；不易开发使用，如开发过程中不能随意指定主键值）
2. uuid等字符串序列（优点：取值唯一；缺点：字符串可能有字母，而且可能有‘-’连接符，连接符去掉需要消耗（虽然可以忽略不计），如果保留连接符，springmvc的path又会有问题....）

结合优缺点，可以使用来自twitter的“雪花算法”进行id的生成，可以保证：（1）可分布式（确保各进程产生唯一序列）；（2）高效；（3）不依赖其它处理或持久化

## 使用
官方（twitter）是Scala实现，除官方实现外，社区也有其它人的java实现，或其它语言实现。

雪花算法的Java实现，[https://github.com/adyliu/idcenter](https://github.com/adyliu/idcenter)
该项目优点是：共3个类，简单；
缺点是：没有文档（当然，因为太简单，所以文档貌似不是必须的）。

### gradle
看域名是sohu，但github上是个人库，可能是sohu的员工
```plain
// https://mvnrepository.com/artifact/com.sohu/idcenter
compile group: 'com.sohu', name: 'idcenter', version: '2.2.2'
```

使用
```plain
// 返回long类型数值id
IdWorker idWoker = new IdWorker();
long id = idWoker.getId();
```


## 参考：
1 雪花算法原理 [https://blog.csdn.net/liwenbo\_csu/article/details/51313555](https://blog.csdn.net/liwenbo_csu/article/details/51313555)

