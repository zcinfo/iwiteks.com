---
title: "智慧旅游——中测的思路与实践"
date: "2016-01-12T19:20:04-07:00"
tags: ["智慧旅游"]
categories: ["blog"]
author: "龙见"
id: "content"
---

#  智慧旅游

“智慧旅游”以物联网、云计算、高性能信息处理、数据挖掘等技术为旅游体验、产业发展、旅游管理等方面的综合应用。

智慧旅游，也被称为“智能旅游”。就是利用云计算、物联网等新技术，通过互联网/移动互联网，借助便携的终端上网设备，主动感知旅游资源、旅游经济、旅游活动、旅游者等方面的信息。通过先进技术的综合利用，信息及时发布与沟通，让旅游的四大主体：主管部门、企业、旅游消费者、当地居民，能够及时了解这些信息，及时安排和调整工作与旅游计划，从而达到对各类旅游信息的智能感知、方便利用的效果。

![](/content/tourism-with-zc-ideas-1.png)

智慧旅游的建设与发展最终将体现在旅游体验、旅游管理、旅游服务和旅游营销等方面，体现为满足各方需求：

1. 满足海量游客的个性化需求。日渐兴盛的旅游市场使得自助游和散客游已经成为一种主要的出游方式。因此，通过“智慧旅游”各项针对旅游消费的应用，将更加便利快捷的满足智能化、个性化、信息化的服务需求量。

2. 实现旅游公共服务与公共管理的无缝整合。随着电子政务向构建数字政府方向发展，智慧旅游与公共服务的结合，意味着海量数据的充分利用、交流与共享，以“公共服务”为中心的服务与管理流程的无缝整合，实现服务与管理决策的科学、合理。

3. 为旅游企业(尤其是中小企业)提供市场服务。基于云计算的智慧旅游平台能够向中小旅游企业提供服务，为其节省信息化建设投资与运营成本，是旅游中小企业进行智慧旅游集约化建设的最佳方式。

# 中测智慧旅游解决方案

中测智慧旅游解决方案，依托云计算技术以及人工智能技术技术，通过信息化、智慧化、科技化的方式，搭建丰富的应用，使旅游业涉及的不同部门和系统之间实现信息共享和协同作业，更合理地利用资源，做出最好的旅游活动和管理决策，面向旅游者、企业、政府和居民提供不同的价值。

例如，中测智慧旅游解决方案，可以让其获取旅游全域/全流程的信息服务，实现出游前的信息查询、合理线路设计、旅游预订、智能导览、门票及优惠券获取、紧急救援、投保理赔等价值。对企业而言，可以获取旅游电子商务、营销、满意度调查、行为追踪、数据统计及挖掘等价值。对政府而言，可以获取行业市场监管、旅游信息与其他公共服务信息共享与协同运作、旅游目的地营销等价值，实现指挥决策、实时反应、协调运作，政府可以更合理地利用资源、做出最优的城市发展和管理决策，形成产业发展与社会管理的新模式。对居民而言，可以享受交通、游憩、休闲等多种系统信息共享的价值。